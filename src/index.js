/* 主进程 */
/* eslint-disable no-undef */
const { app, BrowserWindow } = require('electron');
const path = require('path');
const md5 = require('md5');
// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
    app.quit();
}

const createWindow = () => {
    console.log('打印在IED控制台:' + md5('12345'));

    // Create the browser window.
    const mainWindow = new BrowserWindow({
        width: 1024,
        height: 768,
        webPreferences: {
            // Electron5.x之前默认可以在主进程以及渲染进行中使用nodejs，但是在Electron5.x之后默认无法
            // 在渲染进程中直接使用nodejs，如果想在渲染进程中使用nodejs的需要进行如下配置
            nodeIntegration: true,   // 渲染进程中可以使用nodejs
            enableRemoteModule: true // 启用Remote模块
        }
    });

    // and load the index.html of the app.
    mainWindow.loadFile(path.join(__dirname, 'index.html'));

    // 将一个网站打包为一个软件，缺点是内容无法自定义
    // mainWindow.loadURL('https://gitee.com/');

    // 自定义顶部菜单
    require('./modules/public/menu');
    // 自定义右键菜单
    require('./modules/public/context-menu');
    // 主进程消息监听
    require('./modules/public/message');

    // Open the DevTools.
    mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// 监听electron ready事件创建窗口
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
// 监听窗口关闭的事件，关闭的时候退出应用，macOS需要排除
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    // macOS中点击dock中的应用图标的时候重新创建窗口
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
