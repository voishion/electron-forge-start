/* 渲染进程 */
/* eslint-disable no-undef */
const fs = require("fs");
const md5 = require("md5");

// 引入主进程中的窗口对象
const { ipcRenderer, shell } = require('electron');
// const { remote } = require("electron");
// const BrowserWindow = remote.BrowserWindow;
// const Menu = remote.Menu;

// 页面右键菜单
// const contextMenuTemplate = [
//     { label: '复制', role: 'copy' },
//     { label: '粘贴', role: 'paste' },
//     { type: 'separator' },
//     // 分隔线 
//     { label: '其他功能', click: () => { console.log('click') } }
// ];
// const contextMenu = Menu.buildFromTemplate(contextMenuTemplate);

window.onload = () => {
    // 页面右键监听
    window.addEventListener('contextmenu', (e) => {
        e.preventDefault();
        // contextMenu.popup({ window: remote.getCurrentWindow() });
        // 发送消息给主进程用于弹出右键快捷菜单
        ipcRenderer.send("showContextMenu", "This is Renderer!");
    }, false);

    var btnDom = document.querySelector('#btn');
    var contentDom = document.querySelector('#content');

    btnDom.onclick = () => {
        fs.readFile("src/common/js/test.json", (err, data) => {
            if (err) {
                console.log(err);
                return;
            }
            contentDom.innerHTML = data.toString().replace('_md5_', md5('12345'));
        });
    }

    // Shell功能使用 https://www.electronjs.org/docs/api/shell
    var shellBtns = document.querySelectorAll("a[type]");
    for (let i = 0; i < shellBtns.length; i++) {
        shellBtns[i].onclick = (e) => {
            e.preventDefault(); // 阻止默认事件
            if ("shell-1" === shellBtns[i].getAttribute("type")) {
                shell.openExternal(shellBtns[i].getAttribute("href"));
            } else {
                shell.showItemInFolder(shellBtns[i].getAttribute("href"));
            }
        }
    }

    // RemoteNews
    var btnRemoteNews = document.querySelector('#remoteNews');
    btnRemoteNews.onclick = () => {
        // 通过BrowserWindow打开新窗口（方式一）
        // const mainWindow = new BrowserWindow({
        //     width: 637,
        //     height: 360,
        //     webPreferences: {
        //         nodeIntegration: true
        //     }
        // });
        // mainWindow.loadFile(path.join(__dirname, "modules/news/news.html"));
        // 通过消息通知主进程打开新闻窗口（方式二）
        /**
         * 窗口间数据传递
         * 1、使用H5的localStorage
         * 2、通过消息、主进程实现数据传递
         */
        let newsId = "35492649364910344";
        // localStorage.setItem("newsId", newsId);
        ipcRenderer.send("openNewsWindow", newsId);
    }

    // 渲染进程给主进程发送消息（异步）
    var sendMsgDom = document.querySelector("#sendMsg");
    sendMsgDom.onclick = () => {
        ipcRenderer.send("sendMsg", "this is Renderer msg");
    }

    // 渲染进程给主进程发送消息，主进程给渲染进程反馈消息（异步）=>发送
    var sendMsgReplayDom = document.querySelector("#sendMsgReplay");
    sendMsgReplayDom.onclick = () => {
        ipcRenderer.send("sendMsgReplay", "this is Renderer msg - sendMsgReplay");
    }
    // 渲染进程给主进程发送消息，主进程给渲染进程反馈消息（异步）=>接收【监听主进程的广播反馈】
    ipcRenderer.on("replayRenderer", (event, data) => {
        console.log(data);
    });

    // 渲染进程和主进程通信（同步）
    var sendMsgReplaySyncDom = document.querySelector("#sendMsgSync");
    sendMsgReplaySyncDom.onclick = () => {
        var replay = ipcRenderer.sendSync("sendMsgReplaySync", "this is Renderer msg - sendMsgReplaySync");
        console.log(replay);
    }

    // 监听主进程”查看“菜单->”发送消息至渲染进程“菜单点击时发送的数据
    ipcRenderer.on("sendToRendererMsg", (event, data) => {
        console.log(data);
    });

    // 监听主进程”查看“菜单->”发送消息至渲染进程“菜单点击时发送的数据
    ipcRenderer.on("collectNews", (event, data) => {
        console.log("来自新闻页面收藏按钮点击事件，新闻编号为：" + data);
    });
}