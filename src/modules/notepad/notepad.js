/**
https://www.w3cschool.cn/jsref/event-ondragover.html
ondragenter - 当被鼠标拖动的对象进入其容器范围内时触发此事件
ondragover - 当某被拖动的对象在另一对象容器范围内拖动时触发此事件
ondragleave - 当被鼠标拖动的对象离开其容器范围内时触发此事件
ondrop - 在一个拖动过程中，释放鼠标键时触发此事件
 */
const fs = require("fs");

window.onload = () => {
    var contentDom = document.querySelector('#content');
    // 阻止以下事件的默认行为
    contentDom.ondragenter = contentDom.ondragover = contentDom.ondragleave = () => {
        return false;
    }
    // 
    contentDom.ondrop = (e) => {
        var path = e.dataTransfer.files[0].path;
        fs.readFile
        fs.readFile(path, (err, data) => {
            if (err) {
                console.log(err);
                return;
            }
            contentDom.innerHTML = data.toString();
        });
    }
}