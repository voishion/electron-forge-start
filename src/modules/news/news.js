const { ipcRenderer } = require("electron")

// let newsId = localStorage.getItem("newsId");
// console.log(newsId);

let newsId = null;
ipcRenderer.on("newsId", (event, data) => {
    newsId = data;
});

window.onload = () => {
    var collectBtn = document.querySelector("#collect");
    collectBtn.onclick = () => {
        ipcRenderer.send("collectNews", newsId);
    }
}