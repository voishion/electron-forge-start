/* 右键菜单 */
const { Menu, ipcMain, BrowserWindow } = require("electron");

// 页面右键菜单
const contextMenuTemplate = [
    { label: '复制', role: 'copy' },
    { label: '粘贴', role: 'paste' },
    { type: 'separator' },
    // 分隔线 
    { label: '其他功能', click: () => { console.log('click') } }
];
const contextMenu = Menu.buildFromTemplate(contextMenuTemplate);

// 监听右键菜单消息
ipcMain.on("showContextMenu", (event, data) => {
    // 渲染进程中获取当前窗口的方法 remote.getCurrentWindow() 
    // 主进程中获取当前窗口的方法 BrowserWindow.getFocusedWindow()
    console.log(event);
    console.log(data);
    contextMenu.popup({ window: BrowserWindow.getFocusedWindow() });
});