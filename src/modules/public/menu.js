/* 顶部菜单 */
/* eslint-disable no-undef */
const { app, Menu, BrowserWindow } = require('electron');

const isMac = process.platform === 'darwin';

const template = [
    // appMenu
    ...(isMac ? [{
        label: app.name,
        submenu: [
            {
                label: '关于Electron',
                role: 'about'
            },
            { type: 'separator' },
            {
                label: '服务',
                role: 'services'
            },
            { type: 'separator' },
            {
                label: '隐藏Electron',
                role: 'hide'
            },
            {
                label: '隐藏其他',
                role: 'hideothers'
            },
            {
                label: '全部显示',
                role: 'unhide'
            },
            { type: 'separator' },
            {
                label: '退出Electron',
                role: 'quit'
            }
        ]
    }] : []),
    // fileMenus
    {
        label: '文件',
        submenu: [
            ...(isMac ? [
                {
                    label: "新建",
                    accelerator: "command + n",
                    click: () => {
                        console.log("command+n")
                    }
                },
                { type: 'separator' },
                {
                    label: "打开",
                    accelerator: "command + o",
                    click: () => {
                        console.log("command+o")
                    }
                },
                { type: 'separator' },
                {
                    label: "保存",
                    accelerator: "command + s",
                    click: () => {
                        console.log("command+s")
                    }
                },
                { 
                    label: '关闭窗口', 
                    role: 'close' 
                }
            ] : [
                {
                    label: "新建",
                    accelerator: "ctrl + n",
                    click: () => {
                        console.log("ctrl+n")
                    }
                },
                { type: 'separator' },
                {
                    label: "打开",
                    accelerator: "ctrl + o",
                    click: () => {
                        console.log("ctrl+o")
                    }
                },
                { type: 'separator' },
                {
                    label: "保存",
                    accelerator: "ctrl + s",
                    click: () => {
                        console.log("ctrl+s")
                    }
                },
                { 
                    label: '退出', 
                    role: 'quit' 
                }
            ])
        ]
    },
    // editMenu
    {
        label: '编辑',
        submenu: [
            { label: "撤销", role: 'undo' },
            { label: "恢复", role: 'redo' },
            { type: 'separator' },
            { label: "剪切", role: 'cut' },
            { label: "拷贝", role: 'copy' },
            { label: "粘贴", role: 'paste' },
            { label: "粘贴并匹配格式", role: 'pasteAndMatchStyle' },
            { label: "删除", role: 'delete' },
            { label: "全选", role: 'selectAll' },
            ...(isMac ? [
                { type: 'separator' },
                {
                    label: '语音',
                    submenu: [
                        { label: "开始讲话", role: 'startSpeaking' },
                        { label: "停止讲话", role: 'stopSpeaking' }
                    ]
                }
            ] : [])
        ]
    },
    // viewMenu
    {
        label: '查看',
        submenu: [
            { 
                label: '发送消息至渲染进程', 
                click: () => {
                    BrowserWindow.getFocusedWindow().webContents.send("sendToRendererMsg", "这里是主进程”查看“菜单->”发送消息至渲染进程“菜单");
                } 
            },
            { label: '重载', role: 'reload' },
            { label: '强制重载', role: 'forceReload' },
            { label: '开发工具', role: 'toggleDevTools' },
            { type: 'separator' },
            { label: '缩小', role: 'zoomIn' },
            { label: '放大', role: 'zoomOut' },
            { type: 'separator' },
            { label: '全屏', role: 'togglefullscreen' }
        ]
    },
    // windowMenu
    {
        label: '窗口',
        submenu: [
            { label: '最小化', role: 'minimize' },
            { label: '缩放', role: 'zoom' },
            ...(isMac ? [
                { type: 'separator' },
                { label: '切换窗口...', role: 'window' },
                { type: 'separator' },
                { label: '全部置于顶层', role: 'front' }
            ] : [
                { label: '关闭窗口', role: 'close' }
            ])
        ]
    },
    {
        label: '帮助',
        role: 'help',
        submenu: [
            {
                label: '了解更多',
                click: async () => {
                    const { shell } = require('electron');
                    await shell.openExternal('https://electronjs.org');
                }
            },
            {
                label: '打开百度',
                click: async () => {
                    const { BrowserWindow } = require('electron');
                    BrowserWindow.getFocusedWindow().webContents.send("openIframe", "https://www.baidu.com");
                }
            }
        ]
    }
]

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);