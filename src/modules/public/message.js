/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const { ipcMain, BrowserWindow } = require('electron');
const path = require("path");

/**
 * Windows窗口编号
 */
let WINDOWS_ID = {
    /**
     * 主窗口编号
     */
    INDEX: null,
    /**
     * 新闻窗口编号
     */
    NEWS: null
};

// 接收渲染进程给主进程发送消息（异步）
ipcMain.on("sendMsg", (event, data) => {
    console.log(event);
    console.log(data);
});
// 渲染进程给主进程发送消息，主进程给渲染进程反馈消息（异步）
ipcMain.on("sendMsgReplay", (event, data) => {
    console.log(data);
    event.sender.send("replayRenderer", "主进程已经收到了你发送的消息:" + data);
});
// 渲染进程和主进程通信（同步）
ipcMain.on("sendMsgReplaySync", (event, data) => {
    console.log(data);
    event.returnValue = "我是主进程，我已经收到了你的同步消息：" + data;
});
// 主进程监听渲染进程消息打开新闻窗口
ipcMain.on("openNewsWindow", (event, data) => {
    WINDOWS_ID.INDEX = BrowserWindow.getFocusedWindow().id;
    // 通过BrowserWindow打开新窗口
    const newsWindow = new BrowserWindow({
        width: 637,
        height: 360,
        webPreferences: {
            nodeIntegration: true
        }
    });
    newsWindow.loadFile(path.join(__dirname, "../news/news.html"));
    newsWindow.webContents.on("did-finish-load", (event) => {
        newsWindow.webContents.send("newsId", data);
    });
    WINDOWS_ID.NEWS = newsWindow.id;
});
// 主进程监听新闻窗口消息
ipcMain.on("collectNews", (event, data) => {
    let mainWindow = BrowserWindow.fromId(WINDOWS_ID.INDEX);
    mainWindow.webContents.send("collectNews", data);
});