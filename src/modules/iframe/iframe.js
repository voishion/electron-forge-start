const {ipcRenderer} = require("electron")

ipcRenderer.on("openIframe", (event, url) => {
    var customIframe = document.querySelector("#customIframe");
    customIframe.src = url;
});