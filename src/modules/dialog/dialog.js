var { remote } = require('electron');

//https://electronjs.org/docs/api/dialog
var errorDom = document.querySelector('#error');
var mesageBoxDom = document.querySelector('#mesageBox');
var openDialogDom = document.querySelector('#openDialog');
var saveDialogDom = document.querySelector('#saveDialog');
var mesageBoxSyncDom = document.querySelector('#mesageBoxSync');
var openDialogSyncDom = document.querySelector('#openDialogSync');
var showSaveDialogSyncDom = document.querySelector('#showSaveDialogSync');

errorDom.onclick = function () {
    remote.dialog.showErrorBox("错误！", "数据输入错误");
}

mesageBoxDom.onclick = function () {
    remote.dialog.showMessageBox({
        type: "info",
        // buttons:["ok","no"],
        buttons: ["确定", "取消", "其他"],
        title: "关于我们",
        message: `
        版本: 1.49.1 (user setup)
        提交: 58bb7b2331731bf72587010e943852e13e6fd3cf
        日期: 2020-09-16T23:27:51.792Z
        Electron: 9.2.1
        Chrome: 83.0.4103.122
        Node.js: 12.14.1
        V8: 8.3.110.13-electron.0
        OS: Windows_NT x64 6.1.7601`
    }).then((result) => {
        console.log(result.response);
        if (result.response == 0) {
            console.log("点击了确定");
        } else if (result.response == 1) {
            console.log("点击了取消");
        }
    }).catch((err) => {
        console.log(err);
    });
}

openDialogDom.onclick = function () {
    remote.dialog.showOpenDialog({
        // title: "打开文件",
        // properties: ['openFile'] 
        // properties: ['openFile', 'multiSelections'] 
        title: "打开目录",
        properties: ["openDirectory"]
    }).then((result) => {
        console.log(result.canceled);
        console.log(result.filePaths);
    }).catch((err) => {
        console.log(err);
    });

    console.log("打开文件...");
}

saveDialogDom.onclick = function () {
    remote.dialog.showSaveDialog({
        title: "保存文件",
        defaultPath: "aaa.txt",
        filters: [
            { name: 'Images', extensions: ['jpg', 'png', 'gif'] },
            { name: 'Movies', extensions: ['mkv', 'avi', 'mp4'] },
            { name: 'Custom File Type', extensions: ['as'] },
            { name: 'All Files', extensions: ['*'] }
        ]
    }).then((result) => {
        console.log(result.canceled);
        console.log(result.filePath);

    }).catch((err) => {
        console.log(err);
    })
}

mesageBoxSyncDom.onclick = function () {
    var result = remote.dialog.showMessageBoxSync({
        type: "info",
        buttons: ["ok", "no"],
        title: "关于我们",
        message: `
        版本: 1.49.1 (user setup)
        提交: 58bb7b2331731bf72587010e943852e13e6fd3cf
        日期: 2020-09-16T23:27:51.792Z
        Electron: 9.2.1
        Chrome: 83.0.4103.122
        Node.js: 12.14.1
        V8: 8.3.110.13-electron.0
        OS: Windows_NT x64 6.1.7601`
    })
    console.log(result);

    console.log("111");
}

openDialogSyncDom.onclick = function () {
    var result = remote.dialog.showOpenDialogSync({
        title: "打开目录",
        // properties: ['openFile', 'multiSelections'] 
        // properties: ['openFile'] 
        properties: ["openDirectory"]
    })
    console.log(result);

    console.log("执行");
}

showSaveDialogSyncDom.onclick = function () {
    var result = remote.dialog.showSaveDialogSync({
        title: "保存文件",
        defaultPath: "aaa.txt",
        filters: [
            { name: 'Images', extensions: ['jpg', 'png', 'gif'] },
            { name: 'Movies', extensions: ['mkv', 'avi', 'mp4'] },
            { name: 'Custom File Type', extensions: ['as'] },
            { name: 'All Files', extensions: ['*'] }
        ]
    })
    console.log(result);
    console.log("执行");
}