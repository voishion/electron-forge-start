# electron-forge-start

#### 介绍
基于electron-forge搭建的一个Electron项目
1、进入创建项目的工作空间  
2、安装electron-forge  
`cnpm install -g electron-forge`  
3、创建项目  
`sudo yarn create electron-app electron-forge-nodepad`  
4、修改目录权限  
`sudo chown -R voishion electron-forge-nodepad`  
5、使用VSCode打开项目  
6、在VSCode终端中初始化代码检测  
`eslint --init`  

#### 系统说明  
electron-forge相当于electron的一个脚手架，可以让我们更方便的创建、运行、打包electron项目。

#### VSCode Git代码推送说明  
执行一下命令
```shell
git config --global user.name "Voishion.威森"
git config --global user.email "voishion@foxmail.com"
git config --global credential.helper store
git remote add origin https://gitee.com/voishion/electron-forge-start.git
git pull --rebase origin master
git pull origin master --allow-unrelated-histories
git push -u origin master
```

#### 项目下载后的启动
1、安装依赖  
`npm install => cnpm i # 推荐用后面`  
2、运行项目  
`npm start => cnpm start # 推荐用后面`  
3、初始化代码检测工具  
`eslint --init`

#### 当前学习课程编号  
Next Study:10